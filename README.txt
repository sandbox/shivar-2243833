-- SUMMARY --

Most content on Drupal either has a 'Published on' date or a 'Created on' date 
associated with it. While these are great, sometimes content editors want a date 
they can edit easily for a number of reasons. Display Date is a module that does 
exactly that. It builds on the Date and Date API modules to create an easily 
configurable field that editors will love...

This module will allow content creators to easily set a displayed date for a 
node.  If no date is set, the node created date is used.

-- REQUIREMENTS --

Date
Date API

-- INSTALLATION --

Install as usual, 
see https://drupal.org/documentation/install/modules-themes/modules-7 
for further information.


-- KNOWN ISSUES --
Not simple-tested yet.

-- CREDITS --

Schyler Manning - (schylermanning)
Michael Mongon - (mikmongon)
Shiva Ray - (shivar)
Reuben Wilson - (reujwils)


-- CONTACT --


